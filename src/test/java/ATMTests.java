import ATMSimulator.SpringBootWebApplication;
import ATMSimulator.model.Account;
import ATMSimulator.model.Transaction;
import ATMSimulator.service.AccountService;
import ATMSimulator.service.TransactionService;
import ATMSimulator.service.TransferService;
import ATMSimulator.service.WithdrawService;
import ATMSimulator.util.ScreenConstant;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

/**
 * @author AD
 * @version $Id: ATMTests.java, v 0.1 2019‐12‐19 1:16 PM AD Exp $$
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = {SpringBootWebApplication.class})
public class ATMTests {
    @Autowired
    private AccountService accountService;

    @Autowired
    private TransferService transferService;

    @Autowired
    private WithdrawService withdrawService;

    @Autowired
    private TransactionService transactionService;


    @Test
    public void validateLoginTest() {
        Account account = accountService.validateLogin("123123", "123456");
        Assert.assertEquals(account.getAccountNumber(), "123123");
    }

    @Test
    public void validateLoginTestFail() {
        Account account = accountService.validateLogin("123123", "aaa");
        Assert.assertNull(account);
    }

    @Test
    public void validateLoginTestWrongPassword() {
        Account account = accountService.validateLogin("123123", "123234");
        Assert.assertNull(account);
    }

    @Test
    public void submitFundTransactionTest() {
        String result = transferService.submitFundTransaction("123123", "123456", "123124", 10);
        Assert.assertEquals(result, ScreenConstant.SUCCESS);
    }

    @Test
    public void submitFundTransactionTestFail() {
        String result = transferService.submitFundTransaction("123123", "123123", "123124", 10);
        Assert.assertEquals(result, ScreenConstant.INVALID);
    }

    @Test
    public void submitFundTransactionTestNotEnough() {
        String result = transferService.submitFundTransaction("123125", "123458", "123124", 10);
        Assert.assertEquals(result, ScreenConstant.INSUFFICIENT);
    }

    @Test
    public void calculateWithdrawAmountTest() {
        boolean result = withdrawService.checkValidWithdrawAmount("123123", "123456", 10);
        Assert.assertTrue(result);
    }

    @Test
    public void calculateWithdrawAmountTestFail() {
        boolean result = withdrawService.checkValidWithdrawAmount("123123", "123457", 10);
        Assert.assertFalse(result);
    }

    @Test
    public void calculateWithdrawAmountTestNotEnough() {
        boolean result = withdrawService.checkValidWithdrawAmount("123123", "123456", 200);
        Assert.assertFalse(result);
    }

    @Test
    public void latest10TransactionByAccountTest() {
        List<Transaction> result = transactionService.lastTransaction("123123");
        Assert.assertEquals(result.get(0).getAccountNumber(), "123123");
    }
}
