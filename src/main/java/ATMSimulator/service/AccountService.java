/**
 * Alipay.com Inc.
 * Copyright (c) 2004‐2019 All Rights Reserved.
 */
package ATMSimulator.service;

import ATMSimulator.model.Account;

import java.util.Optional;

/**
 * @author AD
 * @version $Id: AccountService.java, v 0.1 2019‐12‐16 8:49 AM AD Exp $$
 */

public interface AccountService {
    Account validateLogin(String accountNumber, String pin);

    Optional<Account> getAccountByAccountNumber(String accountNumber);

    Account createNewAccount(Account account);
}