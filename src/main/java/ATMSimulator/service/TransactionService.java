package ATMSimulator.service;

import ATMSimulator.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * @author AD
 * @version $Id: TransactionService.java, v 0.1 2019‐12‐16 9:09 AM AD Exp $$
 */
public interface TransactionService {
    List<Transaction> lastTransaction(String accountNumber);

    Page<Transaction> findTransactionPagination(String accountNumber, Pageable pageable);
}