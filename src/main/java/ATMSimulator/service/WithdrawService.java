package ATMSimulator.service;

/**
 * @author AD
 * @version $Id: WithdrawService.java, v 0.1 2019‐12‐16 9:09 AM AD Exp $$
 */
public interface WithdrawService {

    boolean checkValidWithdrawAmount(String accountNumber, String pin, int amount);

}