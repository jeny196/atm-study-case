package ATMSimulator.service.impl;

import ATMSimulator.model.Transaction;
import ATMSimulator.repository.TransactionRepository;
import ATMSimulator.service.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @author AD
 * @version $Id: TransactionServiceImpl.java, v 0.1 2019‐12‐16 9:55 AM AD Exp $$
 */
@Service
@Transactional
public class TransactionServiceImpl implements TransactionService {

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public List<Transaction> lastTransaction(String accountNumber) {
        List<Transaction> transactionList = transactionRepository.findTransactionByAccountNumber(accountNumber);
        return transactionList.size() >= 10
                ? transactionList.subList(transactionList.size() - 10, transactionList.size())
                : transactionList;
    }

    @Override
    public Page<Transaction> findTransactionPagination(String accountNumber, Pageable pageable) {
        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        Sort sort = new Sort(new Sort.Order(Sort.Direction.DESC, "tranId"));
        PageRequest pageRequest = new PageRequest(currentPage, pageSize, sort);
        return transactionRepository.findAllByAccountNumber(accountNumber, pageRequest);
    }
}
