package ATMSimulator.service.impl;

import ATMSimulator.model.Account;
import ATMSimulator.repository.AccountRepository;
import ATMSimulator.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * @author AD
 * @version $Id: AccountService.java, v 0.1 2019‐11‐25 1:57 PM AD Exp $$
 */
@Service
@Transactional
public class AccountServiceImpl implements AccountService {

    @Autowired
    AccountRepository accountRepository;

    public Account validateLogin(String accountNumber, String pin){
        return accountRepository.findAccountByAccountNumberAndPin(accountNumber, pin);
    }

    @Override
    public Optional<Account> getAccountByAccountNumber(String accountNumber) {
        return accountRepository.findAccountByAccountNumber(accountNumber);
    }

    @Override
    public Account createNewAccount(Account account) {
        return accountRepository.save(account);
    }
}
