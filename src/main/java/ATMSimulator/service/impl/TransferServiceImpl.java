package ATMSimulator.service.impl;

import ATMSimulator.model.Account;
import ATMSimulator.model.Transaction;
import ATMSimulator.repository.AccountRepository;
import ATMSimulator.repository.TransactionRepository;
import ATMSimulator.service.TransferService;
import ATMSimulator.util.ScreenConstant;
import ATMSimulator.util.Utils;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.InvalidParameterException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Optional;

import static ATMSimulator.util.Utils.dateTimeFormat;

/**
 * @author AD
 * @version $Id: TransferService.java, v 0.1 2019‐11‐25 1:56 PM AD Exp $$
 */
@Service
@Transactional
public class TransferServiceImpl implements TransferService {

    Logger logger = LogManager.getLogger(TransferServiceImpl.class);

    @Autowired
    AccountRepository accountRepository;
    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public String submitFundTransaction(String accountNumber, String pin, String destination, int amount) throws InvalidParameterException {
        //validate destination
        Account currentAccount = accountRepository.findAccountByAccountNumberAndPin(accountNumber, pin);
        Optional<Account> destinationAccount = accountRepository.findAccountByAccountNumber(destination);

        if (currentAccount == null || destinationAccount == null) {
            return "Account is invalid";
        }
        if (currentAccount.getBalance() <= amount) {
            return "Insufficient balance $" + amount;
        }
        if (amount > 1000) {
            return "Maximum amount to transfer is $1000";
        }

        currentAccount.setBalance(currentAccount.getBalance() - amount);
        destinationAccount.get().setBalance(destinationAccount.get().getBalance() + amount);
        accountRepository.save(Arrays.asList(currentAccount, destinationAccount.get()));
        Transaction transaction = new Transaction();
        transaction.setAccountNumber(accountNumber);
        transaction.setAmount(amount);
        transaction.setRefAccount(destination);
        transaction.setRefNumber(String.valueOf(Utils.generateRandomRef()));
        transaction.setCreatedDate(LocalDateTime.now().format(dateTimeFormat));
        transaction.setTranType(ScreenConstant.TRANSACTION_TRANSFER);
        transactionRepository.save(transaction);
        return ScreenConstant.SUCCESS;
    }
}
