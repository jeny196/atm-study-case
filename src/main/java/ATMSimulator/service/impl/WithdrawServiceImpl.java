package ATMSimulator.service.impl;

import ATMSimulator.model.Account;
import ATMSimulator.model.Transaction;
import ATMSimulator.repository.AccountRepository;
import ATMSimulator.repository.TransactionRepository;
import ATMSimulator.service.WithdrawService;
import ATMSimulator.util.ScreenConstant;
import ATMSimulator.util.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static ATMSimulator.util.Utils.dateTimeFormat;

/**
 * @author AD
 * @version $Id: WithdrawService.java, v 0.1 2019‐11‐25 1:57 PM AD Exp $$
 */
@Service
@Transactional

public class WithdrawServiceImpl implements WithdrawService {

    @Autowired
    AccountRepository accountRepository;

    @Autowired
    TransactionRepository transactionRepository;

    @Override
    public boolean checkValidWithdrawAmount(String accountNumber, String pin, int amount) {
        Account account = accountRepository.findAccountByAccountNumberAndPin(accountNumber, pin);
        if (account != null) {
            if (account.getBalance() >= amount) {
                account.setBalance(account.getBalance() - amount);
                Transaction transaction = new Transaction();
                transaction.setAccountNumber(accountNumber);
                transaction.setAmount(amount);
                transaction.setCreatedDate(LocalDateTime.now().format(dateTimeFormat));
                transaction.setRefNumber(String.valueOf(Utils.generateRandomRef()));
                transaction.setTranType(ScreenConstant.TRANSACTION_WITHDRAW);
                transactionRepository.save(transaction);
                accountRepository.save(account);
                return true;
            }
        }
        return false;
    }
}
