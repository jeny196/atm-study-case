package ATMSimulator.service;

import java.security.InvalidParameterException;

/**
 * @author AD
 * @version $Id: TransferService.java, v 0.1 2019‐12‐16 9:09 AM AD Exp $$
 */
public interface TransferService {
    String submitFundTransaction(String accountNumber, String pin, String destination, int amount) throws InvalidParameterException;
}