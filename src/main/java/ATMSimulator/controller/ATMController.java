package ATMSimulator.controller;

import java.security.Principal;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import ATMSimulator.model.Account;
import ATMSimulator.model.Transaction;
import ATMSimulator.model.dto.TransferDTO;
import ATMSimulator.model.dto.WithdrawDTO;
import ATMSimulator.service.AccountService;
import ATMSimulator.service.TransactionService;
import ATMSimulator.service.TransferService;
import ATMSimulator.service.WithdrawService;
import ATMSimulator.util.ScreenConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
public class ATMController {

    @Autowired
    AccountService accountService;

    @Autowired
    TransferService transferService;

    @Autowired
    WithdrawService withdrawService;

    @Autowired
    TransactionService transactionService;

    private Account currentAccount = null;
    private String amount = null;
    private String refAccount = null;

    @GetMapping("/")
    public String home() {
        return "home";
    }

    @GetMapping("/home")
    public String home1() {
        return "home";
    }

    @GetMapping("/login")
    public String login(Model model) {
        Account account = new Account();
        model.addAttribute("account", account);
        return "login";
    }

    @PostMapping("/login")
    public String login(Model model, @RequestParam("username") String accountNumber, @RequestParam("password") String pin) {
        Account account = accountService.validateLogin(accountNumber, pin);
        model.addAttribute("account", account);
        return "account";
    }

    @GetMapping("/account")
    public String account(HttpServletRequest request, Model model) {
        Principal principal = request.getUserPrincipal();
        Optional<Account> account = accountService.getAccountByAccountNumber(principal.getName());
        if (account.isPresent()) {
            model.addAttribute("name", account.get().getName());
            model.addAttribute("balance", account.get().getBalance());
        }
        return "account";
    }

    @GetMapping("/withdraw")
    public String withdraw(Model model) {
        WithdrawDTO withdrawDTO = new WithdrawDTO();
        model.addAttribute("withdrawDTO", withdrawDTO);

        return "withdraw";
    }

    @PostMapping("/submitWithdraw")
    public String submitWithdraw(HttpServletRequest request, @ModelAttribute("withdrawDTO") WithdrawDTO withdrawDTO) {
        String amount = "";
        String result = "";
        Principal principal = request.getUserPrincipal();
        Optional<Account> account = accountService.getAccountByAccountNumber(principal.getName());

        if (withdrawDTO.getAmount() == null) {
            request.setAttribute("errorMessage", "Error");
            return "withdraw";

        }
        if ("other".equals(withdrawDTO.getAmount())) {
            amount = withdrawDTO.getValue();
        } else {
            amount = withdrawDTO.getAmount();
        }
        String msg = validateAndCalculateWithdrawAmount(1000, amount);
        if (msg == null && account.isPresent()) {
            if (withdrawService.checkValidWithdrawAmount(account.get().getAccountNumber(), account.get().getPin(), Integer.parseInt(amount))) {
                result = "redirect:/account";
            } else {
                request.setAttribute("errorMessage", "Error");
                result = "withdraw";
            }
        } else {
            request.setAttribute("errorMessage", msg);
            result = "withdraw";
        }
        return result;
    }


    @GetMapping("/transaction")
    public String transaction(
            HttpServletRequest request,
            Model model,
            @RequestParam("page") Optional<Integer> page,
            @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(10);
        Principal principal = request.getUserPrincipal();

        Page<Transaction> transactions = transactionService.findTransactionPagination(principal.getName(), new PageRequest(currentPage - 1, pageSize));

        model.addAttribute("transactionsPage", transactions);

        int totalPages = transactions.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "transaction";
    }


    @GetMapping("/transfer")
    public String transfer(Model model) {
        TransferDTO transferDTO = new TransferDTO();
        model.addAttribute("transferDTO", transferDTO);
        return "transfer";
    }

    @PostMapping("/submitTransfer")
    public String submitTransfer(HttpServletRequest request, @ModelAttribute("transferDTO") TransferDTO transferDTO) {
        Principal principal = request.getUserPrincipal();
        Optional<Account> account = accountService.getAccountByAccountNumber(principal.getName());
        String view = "";
        if (account.isPresent()) {
            String result =
                    transferService.submitFundTransaction(
                            account.get().getAccountNumber(),
                            account.get().getPin(),
                            transferDTO.getDestinationAccount(),
                            Integer.parseInt(transferDTO.getAmount())
                    );
            if (ScreenConstant.SUCCESS.equals(result)) {
                view = "redirect:/account";
            } else {
                request.setAttribute("msg", result);
                view = "transfer";
            }
        } else {
            request.setAttribute("msg", "Current Account Not Found");
            view = "transfer";
        }
        return view;

    }

    @GetMapping("/403")
    public String error403() {
        return "/error/403";
    }

    @GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

    public String validateAndCalculateWithdrawAmount(int balance, String amount) {
        if (!amount.matches(ScreenConstant.REGEX_MATCH_NUMBER)) {
            return "Only Number Allowed";
        } else if (Integer.parseInt(amount) % 10 != 0) {
            return "Invalid amount";

        } else if (Integer.parseInt(amount) > 1000) {
            return "Maximum amount to withdraw is $1000";

        } else if (balance - Integer.parseInt(amount) < 0) {
            return "Insufficient balance $" + amount;
        }
        return null;
    }
}