package ATMSimulator.model;

import javax.persistence.*;

/**
 * @author AD
 * @version $Id: Transaction.java, v 0.1 2019‐11‐28 8:52 AM AD Exp $$
 */
@Entity
public class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int tranId;

    private String accountNumber;

    private String refAccount;

    private int amount;

    private String refNumber;

    private String createdDate;

    private int tranType;

    public Transaction() {
    }

    /**
     * Getter method for property accountNumber.
     *
     * @return property value of accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Setter method for property accountNumber.
     *
     * @param accountNumber value to be assigned to property accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }


    /**
     * Getter method for property tranId.
     *
     * @return property value of tranId
     */
    public int getTranId() {
        return tranId;
    }

    /**
     * Setter method for property tranId.
     *
     * @param tranId value to be assigned to property tranId
     */
    public void setTranId(int tranId) {
        this.tranId = tranId;
    }


    /**
     * Getter method for property refAccount.
     *
     * @return property value of refAccount
     */
    public String getRefAccount() {
        return refAccount;
    }

    /**
     * Setter method for property refAccount.
     *
     * @param refAccount value to be assigned to property refAccount
     */
    public void setRefAccount(String refAccount) {
        this.refAccount = refAccount;
    }

    /**
     * Getter method for property amount.
     *
     * @return property value of amount
     */
    public int getAmount() {
        return amount;
    }

    /**
     * Setter method for property amount.
     *
     * @param amount value to be assigned to property amount
     */
    public void setAmount(int amount) {
        this.amount = amount;
    }

    /**
     * Getter method for property refNumber.
     *
     * @return property value of refNumber
     */
    public String getRefNumber() {
        return refNumber;
    }

    /**
     * Setter method for property refNumber.
     *
     * @param refNumber value to be assigned to property refNumber
     */
    public void setRefNumber(String refNumber) {
        this.refNumber = refNumber;
    }

    /**
     * Getter method for property createdDate.
     *
     * @return property value of createdDate
     */
    public String getCreatedDate() {
        return createdDate;
    }

    /**
     * Setter method for property createdDate.
     *
     * @param createdDate value to be assigned to property createdDate
     */
    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    /**
     * Getter method for property tranType.
     *
     * @return property value of tranType
     */
    public int getTranType() {
        return tranType;
    }

    /**
     * Setter method for property tranType.
     *
     * @param tranType value to be assigned to property tranType
     */
    public void setTranType(int tranType) {
        this.tranType = tranType;
    }
}
