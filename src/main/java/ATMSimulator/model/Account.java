package ATMSimulator.model;

import javax.persistence.*;

/**
 * @author AD
 * @version $Id: ATM.java, v 0.1 2019‐11‐06 9:23 AM AD Exp $$
 */
@Entity
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int accountId;

    private String name;

    private String pin;

    private int amount;

    private String accountNumber;

    public Account() {
    }


    /**
     * Getter method for property name.
     *
     * @return property value of name
     */
    public String getName() {
        return name;
    }

    /**
     * Setter method for property name.
     *
     * @param name value to be assigned to property name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Getter method for property pin.
     *
     * @return property value of pin
     */
    public String getPin() {
        return pin;
    }

    /**
     * Setter method for property pin.
     *
     * @param pin value to be assigned to property pin
     */
    public void setPin(String pin) {
        this.pin = pin;
    }

    /**
     * Getter method for property balance.
     *
     * @return property value of balance
     */
    public int getBalance() {
        return amount;
    }

    /**
     * Setter method for property balance.
     *
     * @param balance value to be assigned to property balance
     */
    public void setBalance(int balance) {
        this.amount = balance;
    }

    /**
     * Getter method for property accountNumber.
     *
     * @return property value of accountNumber
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Setter method for property accountNumber.
     *
     * @param accountNumber value to be assigned to property accountNumber
     */
    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    /**
     * Getter method for property accountId.
     *
     * @return property value of accountId
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Setter method for property accountId.
     *
     * @param accountId value to be assigned to property accountId
     */
    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }
}
