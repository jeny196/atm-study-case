package ATMSimulator.repository;

import ATMSimulator.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * @author AD
 * @version $Id: AccountRepository.java, v 0.1 2019‐12‐16 8:36 AM AD Exp $$
 */
@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

    Optional<Account> findAccountByAccountNumber(String accountNumber);

    Account findAccountByAccountNumberAndPin (String accountNumber, String pin);

}