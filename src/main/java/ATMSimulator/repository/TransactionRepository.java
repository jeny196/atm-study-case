package ATMSimulator.repository;

import ATMSimulator.model.Transaction;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author AD
 * @version $Id: TransactionRepository.java, v 0.1 2019‐12‐16 8:41 AM AD Exp $$
 */
@Repository
public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

    List<Transaction> findTransactionByAccountNumber(String accountNumber);

    Page<Transaction> findAllByAccountNumber(String accountNumber, Pageable pageable);

}