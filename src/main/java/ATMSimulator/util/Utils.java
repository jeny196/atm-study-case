package ATMSimulator.util;

import java.time.format.DateTimeFormatter;

/**
 * @author AD
 * @version $Id: Utils.java, v 0.1 2019‐12‐16 10:11 AM AD Exp $$
 */
public class Utils {

    public static DateTimeFormatter dateTimeFormat = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm a");

    public static int generateRandomRef() {
        return (int) (Math.random() * ((999999 - 100000) + 1)) + 100000;
    }

}
