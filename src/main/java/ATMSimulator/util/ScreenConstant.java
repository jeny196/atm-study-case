package ATMSimulator.util;

/**
 * @author AD
 * @version $Id: SceenConstant.java, v 0.1 2019‐11‐28 4:30 PM AD Exp $$
 */
public class ScreenConstant {
    public static final String REGEX_MATCH_NUMBER = "[0-9]+";
    public static final int TRANSACTION_WITHDRAW = 1;
    public static final int TRANSACTION_TRANSFER = 2;
    public static final String SUCCESS = "SUCCESS";
    public static final String INVALID = "Account is invalid";
    public static final String INSUFFICIENT = "Insufficient balance $10";

}
