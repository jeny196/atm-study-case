-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th1 10, 2020 lúc 08:12 AM
-- Phiên bản máy phục vụ: 5.6.42-log
-- Phiên bản PHP: 7.3.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `atm`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `account`
--

CREATE TABLE `account` (
  `account_id` int(11) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `pin` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `account`
--

INSERT INTO `account` (`account_id`, `account_number`, `amount`, `name`, `pin`) VALUES
(1, '123123', 100, 'User 1', '123456'),
(2, '123124', 100, 'User 2', '123457'),
(3, '123125', 0, 'User 3', '123458'),
(4, '123126', 100, 'User 4', '123459'),
(5, '123127', 100, 'User 5', '123450');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `transaction`
--

CREATE TABLE `transaction` (
  `tran_id` int(11) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `amount` int(11) NOT NULL,
  `created_date` varchar(255) DEFAULT NULL,
  `ref_account` varchar(255) DEFAULT NULL,
  `ref_number` varchar(255) DEFAULT NULL,
  `tran_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Đang đổ dữ liệu cho bảng `transaction`
--

INSERT INTO `transaction` (`tran_id`, `account_number`, `amount`, `created_date`, `ref_account`, `ref_number`, `tran_type`) VALUES
(1, '123123', 10, '2019-12-18T13:34:34.621', NULL, '455004', 1),
(2, '123124', 10, '2019-12-18T13:45:09.845', '123123', '518582', 2),
(3, '123124', 10, '2019-12-18T13:54:39.201', NULL, '780891', 1),
(4, '123124', 10, '2019-12-18T13:56:22.207', NULL, '334540', 1),
(5, '123125', 50, '2019-12-18T13:58:40.297', NULL, '367102', 1),
(6, '123125', 10, '2019/12/18 13:59 PM', NULL, '133703', 1),
(7, '123125', 10, '2019/12/18 14:00 PM', '123123', '572822', 2),
(8, '123125', 20, '2019/12/18 14:00 PM', NULL, '814846', 1),
(9, '123125', 10, '2019/12/18 14:45 PM', NULL, '917624', 1),
(10, '123124', 10, '2019/12/18 14:48 PM', NULL, '630880', 1),
(11, '123126', 10, '2019/12/18 14:49 PM', '123123', '468505', 2),
(12, '123126', 10, '2019/12/18 14:49 PM', '123123', '825081', 2),
(13, '123126', 10, '2019/12/18 14:56 PM', '123123', '186055', 2),
(14, '123123', 10, '2019/12/19 13:24 PM', NULL, '634859', 1),
(15, '123123', 10, '2019/12/19 13:24 PM', '123124', '218643', 2),
(16, '123123', 10, '2020/01/08 13:04 PM', NULL, '900650', 1),
(17, '123123', 10, '2020/01/08 13:04 PM', '123124', '619974', 2),
(18, '123123', 10, '2020/01/10 13:51 PM', NULL, '141902', 1),
(19, '123123', 10, '2020/01/10 13:51 PM', '123124', '606645', 2),
(20, '123123', 10, '2020/01/10 14:06 PM', NULL, '163560', 1),
(21, '123123', 10, '2020/01/10 14:06 PM', '123124', '513293', 2),
(22, '123123', 10, '2020/01/10 14:08 PM', NULL, '806196', 1),
(23, '123124', 10, '2020/01/10 14:10 PM', NULL, '463778', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `account`
--
ALTER TABLE `account`
  ADD PRIMARY KEY (`account_id`);

--
-- Chỉ mục cho bảng `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`tran_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `account`
--
ALTER TABLE `account`
  MODIFY `account_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `transaction`
--
ALTER TABLE `transaction`
  MODIFY `tran_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
